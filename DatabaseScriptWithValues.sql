USE [MovieAPIFinal]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 9/2/2020 10:40:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Actors]    Script Date: 9/2/2020 10:40:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[OtherNames] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[Gender] [nvarchar](max) NULL,
	[DateOfBirth] [datetime2](7) NOT NULL,
	[PlaceOfBirth] [nvarchar](max) NULL,
	[Biography] [nvarchar](max) NULL,
	[PictureUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_Actors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Characters]    Script Date: 9/2/2020 10:40:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Characters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[Gender] [nvarchar](max) NULL,
	[PictureUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_Characters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Franchises]    Script Date: 9/2/2020 10:40:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Franchises](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Franchises] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movie]    Script Date: 9/2/2020 10:40:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movie](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MovieTitle] [nvarchar](max) NULL,
	[Genres] [nvarchar](max) NULL,
	[ReleaseYear] [datetime2](7) NOT NULL,
	[Director] [nvarchar](max) NULL,
	[PictureUrl] [nvarchar](max) NULL,
	[Trailer] [nvarchar](max) NULL,
	[FranchiseId] [int] NOT NULL,
 CONSTRAINT [PK_Movie] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MovieCharacter]    Script Date: 9/2/2020 10:40:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovieCharacter](
	[ActorId] [int] NOT NULL,
	[CharacterId] [int] NOT NULL,
	[MovieId] [int] NOT NULL,
	[PictureUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_MovieCharacter] PRIMARY KEY CLUSTERED 
(
	[MovieId] ASC,
	[CharacterId] ASC,
	[ActorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200902074322_Initial', N'3.1.7')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200902074423_values', N'3.1.7')
GO
SET IDENTITY_INSERT [dbo].[Actors] ON 

INSERT [dbo].[Actors] ([Id], [FirstName], [OtherNames], [LastName], [Gender], [DateOfBirth], [PlaceOfBirth], [Biography], [PictureUrl]) VALUES (1, N'George', NULL, N'Clooney', N'Male', CAST(N'1961-01-01T00:00:00.0000000' AS DateTime2), N'Kentucky', N'George Timothy Clooney (born May 6, 1961) is an American actor, film director, producer, and screenwriter. ', N'https://en.wikipedia.org/wiki/George_Clooney#/media/File:George_Clooney_2016.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [OtherNames], [LastName], [Gender], [DateOfBirth], [PlaceOfBirth], [Biography], [PictureUrl]) VALUES (2, N'Viggo', NULL, N'Mortensen', N'Male', CAST(N'1957-01-01T00:00:00.0000000' AS DateTime2), N'Manhattan', N' Viggo Peter Mortensen jr. (født 20.oktober 1958 i New York City) er en dansk - amerikansk Oscar - og Golden Globe - nominert skuespiller, poet, fotograf og maler, mest kjent for rollen som «Aragorn» i Ringenes herre - filmtrilogien.', N'https://no.wikipedia.org/wiki/Viggo_Mortensen#/media/Fil:Viggo_Mortensen_Cannes_2016.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [OtherNames], [LastName], [Gender], [DateOfBirth], [PlaceOfBirth], [Biography], [PictureUrl]) VALUES (3, N'Christian', NULL, N'Bale', N'Male', CAST(N'1970-01-01T00:00:00.0000000' AS DateTime2), N'HaverFordWest', N'Christian Charles Philip Bale (født 30. januar 1974) er en britisk skuespiller. Han er kjent for sine roller i Terminator Salvation, American Psycho, Solens rike, Batman Begins, The Dark Knight, The Dark Knight Rises, The Fighter og American Hustle.', N'https://no.wikipedia.org/wiki/Christian_Bale#/media/Fil:Christian_Bale-7837.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [OtherNames], [LastName], [Gender], [DateOfBirth], [PlaceOfBirth], [Biography], [PictureUrl]) VALUES (4, N'Arnold', NULL, N'Scwarchneger', N'Male', CAST(N'1957-01-01T00:00:00.0000000' AS DateTime2), N'Austria', N'Arnold Alois Schwarzenegger  is an Austrian-American actor, businessman, former politician and professional bodybuilder.', N'https://en.wikipedia.org/wiki/Arnold_Schwarzenegger#/media/File:Arnold_Schwarzenegger_by_Gage_Skidmore_4.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [OtherNames], [LastName], [Gender], [DateOfBirth], [PlaceOfBirth], [Biography], [PictureUrl]) VALUES (5, N'Keira', NULL, N'Knightley', N'Female', CAST(N'2018-03-29T00:34:00.0000000' AS DateTime2), N'America', N'American actress', N'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/KeiraKnightleyByAndreaRaffin2011.jpg/375px-KeiraKnightleyByAndreaRaffin2011.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [OtherNames], [LastName], [Gender], [DateOfBirth], [PlaceOfBirth], [Biography], [PictureUrl]) VALUES (6, N'Keira', NULL, N'Knightley', N'Female', CAST(N'2018-03-29T00:34:00.0000000' AS DateTime2), N'America', N'American actress', N'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/KeiraKnightleyByAndreaRaffin2011.jpg/375px-KeiraKnightleyByAndreaRaffin2011.jpg')
SET IDENTITY_INSERT [dbo].[Actors] OFF
GO
SET IDENTITY_INSERT [dbo].[Characters] ON 

INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [PictureUrl]) VALUES (1, N'Batman', N'Batman', N'Male', N'https://no.wikipedia.org/wiki/Batman#/media/Fil:Batman_(black_background).jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [PictureUrl]) VALUES (2, N'Aragorn', N'Elessar', N'Male', N'https://commons.wikimedia.org/wiki/File:Aragorn.png')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [PictureUrl]) VALUES (3, N'Victor Fries', N'Mr Freeze', N'Male', N'https://en.wikipedia.org/wiki/Mr._Freeze#/media/File:Mr._Freeze_Batman_Annual_Vol_2_1.png')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [PictureUrl]) VALUES (4, N'Terminator', N'Onkel Bob', N'Male', N'https://upload.wikimedia.org/wikipedia/en/7/70/Terminator1984movieposter.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [PictureUrl]) VALUES (5, N'Harry Potter', N'The half blood prince', N'Male', N'https://image.side2.no/4458485.jpg?imageId=4458485&x=0&y=0&cropw=100&croph=100&width=480&height=270')
SET IDENTITY_INSERT [dbo].[Characters] OFF
GO
SET IDENTITY_INSERT [dbo].[Franchises] ON 

INSERT [dbo].[Franchises] ([Id], [Name], [Description]) VALUES (1, N'The lord of the rings ', N'An epic fantasy saga, based on the books by JRR Tolkien')
INSERT [dbo].[Franchises] ([Id], [Name], [Description]) VALUES (2, N'Batman ', N'The story about the masked vigilante')
INSERT [dbo].[Franchises] ([Id], [Name], [Description]) VALUES (3, N'Terminator ', N'A terminator is sent back in time to save the future')
INSERT [dbo].[Franchises] ([Id], [Name], [Description]) VALUES (4, N'Harry Potter ', N'The story about a young wizard')
SET IDENTITY_INSERT [dbo].[Franchises] OFF
GO
SET IDENTITY_INSERT [dbo].[Movie] ON 

INSERT [dbo].[Movie] ([Id], [MovieTitle], [Genres], [ReleaseYear], [Director], [PictureUrl], [Trailer], [FranchiseId]) VALUES (1, N'The fellowship of the ring', N'Adventure', CAST(N'2001-01-01T00:00:00.0000000' AS DateTime2), N'Peter Jackson', N'https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i', N'https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i', 1)
INSERT [dbo].[Movie] ([Id], [MovieTitle], [Genres], [ReleaseYear], [Director], [PictureUrl], [Trailer], [FranchiseId]) VALUES (2, N'Batman and Robin', N'Action', CAST(N'1988-01-01T00:00:00.0000000' AS DateTime2), N'Joel Schumacker', N'https://en.wikipedia.org/wiki/Batman_%26_Robin_(film)#/media/File:Batman_&_Robin_poster.jpg', N'https://www.youtube.com/watch?v=4RBXypX4qWI', 2)
INSERT [dbo].[Movie] ([Id], [MovieTitle], [Genres], [ReleaseYear], [Director], [PictureUrl], [Trailer], [FranchiseId]) VALUES (3, N'The dark knight', N'Action', CAST(N'2008-01-01T00:00:00.0000000' AS DateTime2), N'Christopher Nolan', N'https://en.wikipedia.org/wiki/The_Dark_Knight_(film)#/media/File:Dark_Knight.jpg', N'https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i', 2)
INSERT [dbo].[Movie] ([Id], [MovieTitle], [Genres], [ReleaseYear], [Director], [PictureUrl], [Trailer], [FranchiseId]) VALUES (4, N'The Terminator', N'Action', CAST(N'1987-01-01T00:00:00.0000000' AS DateTime2), N'James Cameron', N'https://upload.wikimedia.org/wikipedia/en/7/70/Terminator1984movieposter.jpg', N'https://www.youtube.com/watch?v=k64P4l2Wmeg', 3)
INSERT [dbo].[Movie] ([Id], [MovieTitle], [Genres], [ReleaseYear], [Director], [PictureUrl], [Trailer], [FranchiseId]) VALUES (5, N'Harry Potter and the philosophers stone', N'Adventure', CAST(N'2002-01-01T00:00:00.0000000' AS DateTime2), N'Christoffer Columbus', N'https://upload.wikimedia.org/wikipedia/en/6/6b/Harry_Potter_and_the_Philosopher%27s_Stone_Book_Cover.jpg', N'https://www.youtube.com/watch?v=VyHV0BRtdxo', 4)
SET IDENTITY_INSERT [dbo].[Movie] OFF
GO
INSERT [dbo].[MovieCharacter] ([ActorId], [CharacterId], [MovieId], [PictureUrl]) VALUES (2, 2, 1, NULL)
INSERT [dbo].[MovieCharacter] ([ActorId], [CharacterId], [MovieId], [PictureUrl]) VALUES (1, 1, 2, NULL)
INSERT [dbo].[MovieCharacter] ([ActorId], [CharacterId], [MovieId], [PictureUrl]) VALUES (4, 3, 2, NULL)
INSERT [dbo].[MovieCharacter] ([ActorId], [CharacterId], [MovieId], [PictureUrl]) VALUES (3, 1, 3, NULL)
INSERT [dbo].[MovieCharacter] ([ActorId], [CharacterId], [MovieId], [PictureUrl]) VALUES (4, 4, 4, NULL)
INSERT [dbo].[MovieCharacter] ([ActorId], [CharacterId], [MovieId], [PictureUrl]) VALUES (5, 5, 5, NULL)
GO
ALTER TABLE [dbo].[Movie]  WITH CHECK ADD  CONSTRAINT [FK_Movie_Franchises_FranchiseId] FOREIGN KEY([FranchiseId])
REFERENCES [dbo].[Franchises] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Movie] CHECK CONSTRAINT [FK_Movie_Franchises_FranchiseId]
GO
ALTER TABLE [dbo].[MovieCharacter]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacter_Actors_ActorId] FOREIGN KEY([ActorId])
REFERENCES [dbo].[Actors] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacter] CHECK CONSTRAINT [FK_MovieCharacter_Actors_ActorId]
GO
ALTER TABLE [dbo].[MovieCharacter]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacter_Characters_CharacterId] FOREIGN KEY([CharacterId])
REFERENCES [dbo].[Characters] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacter] CHECK CONSTRAINT [FK_MovieCharacter_Characters_CharacterId]
GO
ALTER TABLE [dbo].[MovieCharacter]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacter_Movie_MovieId] FOREIGN KEY([MovieId])
REFERENCES [dbo].[Movie] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacter] CHECK CONSTRAINT [FK_MovieCharacter_Movie_MovieId]
GO
