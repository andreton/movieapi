#In this project I have created a RESTful API for a movie database

##Dotnet core is used, along with SQL server and postman for testing the solution.

Through the project I have had some problems with managing the seeding of data in to the database. The final version works, but due to these problems I have added a folder called Database-scripts in the project, which can be run in order to create the table if problems arise. (I hope it won't be necessary). 

A postman collection is also added. Here I have populated the body with JSON for commands to be used when running the API. (Note that I have designed these commands to work with the seeded data from the database).

