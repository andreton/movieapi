﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MovieAPI
{
    public class Movie
    {
        
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public string Genres { get; set; }
        public DateTime ReleaseYear { get; set; }
        public string Director { get; set; }
        public string PictureUrl { get; set; }
        public string Trailer { get; set; }
        /*Adding many to many between movies and characters*/
        public ICollection<MovieCharacter>MovieCharacters { get; set; }

        /*Franchise, one to many */ 
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
