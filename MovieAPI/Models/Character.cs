﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI
{
    public class Character
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }

        public string Gender { get; set; }
        public string PictureUrl { get; set; }

        /*Adding many to many between characters and movies */
        public ICollection<MovieCharacter> MovieCharacters { get; set; }

    }
}
