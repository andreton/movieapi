﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace MovieAPI
{
    public class Actor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string OtherNames { get; set; }
        public string LastName { get; set; }

        public string Gender { get; set; }

        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Biography { get; set; }

        public string PictureUrl { get; set; }
        
        //Adding one to many from moviecharacter to actor
        public ICollection<MovieCharacter> MovieCharacters { get; set; } 

    }
}
