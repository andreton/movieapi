﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MovieAPI
{
    public class Franchise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        /*Many to one relation between franchise and movie*/
        public ICollection<Movie> Movies { get; set; }
    }
}
