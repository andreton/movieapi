﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MovieAPI
{
    public class MovieCharacter
    {
        public string PictureUrl { get; set; }

        /*This class works as the binding many to many entity for actor, character and movie*/

        /*Actor*/
        public int ActorId { get; set; }
        public Actor Actor { get; set; }

        /*Character*/
        public int CharacterId { get; set; }
        public Character Character { get; set; }

        /*Movie*/
        public int MovieId { get; set; }
        public  Movie Movie{ get; set; }
    }
}
