﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MovieAPI;

namespace MovieAPI.Migrations
{
    [DbContext(typeof(MovieCharacterDBContext))]
    partial class MovieCharacterDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.7")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MovieAPI.Actor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Biography")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("DateOfBirth")
                        .HasColumnType("datetime2");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Gender")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("OtherNames")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PictureUrl")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PlaceOfBirth")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Actors");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Biography = "George Timothy Clooney (born May 6, 1961) is an American actor, film director, producer, and screenwriter. ",
                            DateOfBirth = new DateTime(1961, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            FirstName = "George",
                            Gender = "Male",
                            LastName = "Clooney",
                            PictureUrl = "https://en.wikipedia.org/wiki/George_Clooney#/media/File:George_Clooney_2016.jpg",
                            PlaceOfBirth = "Kentucky"
                        },
                        new
                        {
                            Id = 2,
                            Biography = " Viggo Peter Mortensen jr. (født 20.oktober 1958 i New York City) er en dansk - amerikansk Oscar - og Golden Globe - nominert skuespiller, poet, fotograf og maler, mest kjent for rollen som «Aragorn» i Ringenes herre - filmtrilogien.",
                            DateOfBirth = new DateTime(1957, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            FirstName = "Viggo",
                            Gender = "Male",
                            LastName = "Mortensen",
                            PictureUrl = "https://no.wikipedia.org/wiki/Viggo_Mortensen#/media/Fil:Viggo_Mortensen_Cannes_2016.jpg",
                            PlaceOfBirth = "Manhattan"
                        },
                        new
                        {
                            Id = 3,
                            Biography = "Christian Charles Philip Bale (født 30. januar 1974) er en britisk skuespiller. Han er kjent for sine roller i Terminator Salvation, American Psycho, Solens rike, Batman Begins, The Dark Knight, The Dark Knight Rises, The Fighter og American Hustle.",
                            DateOfBirth = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            FirstName = "Christian",
                            Gender = "Male",
                            LastName = "Bale",
                            PictureUrl = "https://no.wikipedia.org/wiki/Christian_Bale#/media/Fil:Christian_Bale-7837.jpg",
                            PlaceOfBirth = "HaverFordWest"
                        },
                        new
                        {
                            Id = 4,
                            Biography = "Arnold Alois Schwarzenegger  is an Austrian-American actor, businessman, former politician and professional bodybuilder.",
                            DateOfBirth = new DateTime(1957, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            FirstName = "Arnold",
                            Gender = "Male",
                            LastName = "Scwarchneger",
                            PictureUrl = "https://en.wikipedia.org/wiki/Arnold_Schwarzenegger#/media/File:Arnold_Schwarzenegger_by_Gage_Skidmore_4.jpg",
                            PlaceOfBirth = "Austria"
                        },
                        new
                        {
                            Id = 5,
                            Biography = "Daniel Jacob Radcliffe (født 23. juli 1989 i Fulham i London) er en britisk skuespiller som er mest kjent for sin rolle som «Harry Potter», hovedrollefiguren i filmen Harry Potter og de vises stein og dens oppfølgere",
                            DateOfBirth = new DateTime(1989, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            FirstName = "Daniel",
                            Gender = "Male",
                            LastName = "Radcliffe",
                            PictureUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Daniel_Radcliffe_SDCC_2014.jpg/375px-Daniel_Radcliffe_SDCC_2014.jpg",
                            PlaceOfBirth = "England"
                        });
                });

            modelBuilder.Entity("MovieAPI.Character", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Alias")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FullName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Gender")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PictureUrl")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Characters");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Alias = "Batman",
                            FullName = "Batman",
                            Gender = "Male",
                            PictureUrl = "https://no.wikipedia.org/wiki/Batman#/media/Fil:Batman_(black_background).jpg"
                        },
                        new
                        {
                            Id = 2,
                            Alias = "Elessar",
                            FullName = "Aragorn",
                            Gender = "Male",
                            PictureUrl = "https://commons.wikimedia.org/wiki/File:Aragorn.png"
                        },
                        new
                        {
                            Id = 3,
                            Alias = "Mr Freeze",
                            FullName = "Victor Fries",
                            Gender = "Male",
                            PictureUrl = "https://en.wikipedia.org/wiki/Mr._Freeze#/media/File:Mr._Freeze_Batman_Annual_Vol_2_1.png"
                        },
                        new
                        {
                            Id = 4,
                            Alias = "Onkel Bob",
                            FullName = "Terminator",
                            Gender = "Male",
                            PictureUrl = "https://upload.wikimedia.org/wikipedia/en/7/70/Terminator1984movieposter.jpg"
                        },
                        new
                        {
                            Id = 5,
                            Alias = "The half blood prince",
                            FullName = "Harry Potter",
                            Gender = "Male",
                            PictureUrl = "https://image.side2.no/4458485.jpg?imageId=4458485&x=0&y=0&cropw=100&croph=100&width=480&height=270"
                        });
                });

            modelBuilder.Entity("MovieAPI.Franchise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Franchises");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "An epic fantasy saga, based on the books by JRR Tolkien",
                            Name = "The lord of the rings "
                        },
                        new
                        {
                            Id = 2,
                            Description = "The story about the masked vigilante",
                            Name = "Batman "
                        },
                        new
                        {
                            Id = 3,
                            Description = "A terminator is sent back in time to save the future",
                            Name = "Terminator "
                        },
                        new
                        {
                            Id = 4,
                            Description = "The story about a young wizard",
                            Name = "Harry Potter "
                        });
                });

            modelBuilder.Entity("MovieAPI.Movie", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Director")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("FranchiseId")
                        .HasColumnType("int");

                    b.Property<string>("Genres")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("MovieTitle")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PictureUrl")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ReleaseYear")
                        .HasColumnType("datetime2");

                    b.Property<string>("Trailer")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("FranchiseId");

                    b.ToTable("Movie");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Director = "Peter Jackson",
                            FranchiseId = 1,
                            Genres = "Adventure",
                            MovieTitle = "The fellowship of the ring",
                            PictureUrl = "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i",
                            ReleaseYear = new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Trailer = "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i"
                        },
                        new
                        {
                            Id = 2,
                            Director = "Joel Schumacker",
                            FranchiseId = 2,
                            Genres = "Action",
                            MovieTitle = "Batman and Robin",
                            PictureUrl = "https://en.wikipedia.org/wiki/Batman_%26_Robin_(film)#/media/File:Batman_&_Robin_poster.jpg",
                            ReleaseYear = new DateTime(1988, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Trailer = "https://www.youtube.com/watch?v=4RBXypX4qWI"
                        },
                        new
                        {
                            Id = 3,
                            Director = "Christopher Nolan",
                            FranchiseId = 2,
                            Genres = "Action",
                            MovieTitle = "The dark knight",
                            PictureUrl = "https://en.wikipedia.org/wiki/The_Dark_Knight_(film)#/media/File:Dark_Knight.jpg",
                            ReleaseYear = new DateTime(2008, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Trailer = "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i"
                        },
                        new
                        {
                            Id = 4,
                            Director = "James Cameron",
                            FranchiseId = 3,
                            Genres = "Action",
                            MovieTitle = "The Terminator",
                            PictureUrl = "https://upload.wikimedia.org/wikipedia/en/7/70/Terminator1984movieposter.jpg",
                            ReleaseYear = new DateTime(1987, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Trailer = "https://www.youtube.com/watch?v=k64P4l2Wmeg"
                        },
                        new
                        {
                            Id = 5,
                            Director = "Christoffer Columbus",
                            FranchiseId = 4,
                            Genres = "Adventure",
                            MovieTitle = "Harry Potter and the philosophers stone",
                            PictureUrl = "https://upload.wikimedia.org/wikipedia/en/6/6b/Harry_Potter_and_the_Philosopher%27s_Stone_Book_Cover.jpg",
                            ReleaseYear = new DateTime(2002, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo"
                        });
                });

            modelBuilder.Entity("MovieAPI.MovieCharacter", b =>
                {
                    b.Property<int>("MovieId")
                        .HasColumnType("int");

                    b.Property<int>("CharacterId")
                        .HasColumnType("int");

                    b.Property<int>("ActorId")
                        .HasColumnType("int");

                    b.Property<string>("PictureUrl")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("MovieId", "CharacterId", "ActorId");

                    b.HasIndex("ActorId");

                    b.HasIndex("CharacterId");

                    b.ToTable("MovieCharacter");

                    b.HasData(
                        new
                        {
                            MovieId = 2,
                            CharacterId = 1,
                            ActorId = 1
                        },
                        new
                        {
                            MovieId = 1,
                            CharacterId = 2,
                            ActorId = 2
                        },
                        new
                        {
                            MovieId = 3,
                            CharacterId = 1,
                            ActorId = 3
                        },
                        new
                        {
                            MovieId = 2,
                            CharacterId = 3,
                            ActorId = 4
                        },
                        new
                        {
                            MovieId = 4,
                            CharacterId = 4,
                            ActorId = 4
                        },
                        new
                        {
                            MovieId = 5,
                            CharacterId = 5,
                            ActorId = 5
                        });
                });

            modelBuilder.Entity("MovieAPI.Movie", b =>
                {
                    b.HasOne("MovieAPI.Franchise", "Franchise")
                        .WithMany("Movies")
                        .HasForeignKey("FranchiseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MovieAPI.MovieCharacter", b =>
                {
                    b.HasOne("MovieAPI.Actor", "Actor")
                        .WithMany("MovieCharacters")
                        .HasForeignKey("ActorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MovieAPI.Character", "Character")
                        .WithMany("MovieCharacters")
                        .HasForeignKey("CharacterId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MovieAPI.Movie", "Movie")
                        .WithMany("MovieCharacters")
                        .HasForeignKey("MovieId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
