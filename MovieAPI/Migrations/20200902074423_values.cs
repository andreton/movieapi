﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieAPI.Migrations
{
    public partial class values : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Biography", "DateOfBirth", "FirstName", "Gender", "LastName", "OtherNames", "PictureUrl", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 1, "George Timothy Clooney (born May 6, 1961) is an American actor, film director, producer, and screenwriter. ", new DateTime(1961, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "George", "Male", "Clooney", null, "https://en.wikipedia.org/wiki/George_Clooney#/media/File:George_Clooney_2016.jpg", "Kentucky" },
                    { 2, " Viggo Peter Mortensen jr. (født 20.oktober 1958 i New York City) er en dansk - amerikansk Oscar - og Golden Globe - nominert skuespiller, poet, fotograf og maler, mest kjent for rollen som «Aragorn» i Ringenes herre - filmtrilogien.", new DateTime(1957, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Viggo", "Male", "Mortensen", null, "https://no.wikipedia.org/wiki/Viggo_Mortensen#/media/Fil:Viggo_Mortensen_Cannes_2016.jpg", "Manhattan" },
                    { 3, "Christian Charles Philip Bale (født 30. januar 1974) er en britisk skuespiller. Han er kjent for sine roller i Terminator Salvation, American Psycho, Solens rike, Batman Begins, The Dark Knight, The Dark Knight Rises, The Fighter og American Hustle.", new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Christian", "Male", "Bale", null, "https://no.wikipedia.org/wiki/Christian_Bale#/media/Fil:Christian_Bale-7837.jpg", "HaverFordWest" },
                    { 4, "Arnold Alois Schwarzenegger  is an Austrian-American actor, businessman, former politician and professional bodybuilder.", new DateTime(1957, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Arnold", "Male", "Scwarchneger", null, "https://en.wikipedia.org/wiki/Arnold_Schwarzenegger#/media/File:Arnold_Schwarzenegger_by_Gage_Skidmore_4.jpg", "Austria" },
                    { 5, "Daniel Jacob Radcliffe (født 23. juli 1989 i Fulham i London) er en britisk skuespiller som er mest kjent for sin rolle som «Harry Potter», hovedrollefiguren i filmen Harry Potter og de vises stein og dens oppfølgere", new DateTime(1989, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Daniel", "Male", "Radcliffe", null, "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Daniel_Radcliffe_SDCC_2014.jpg/375px-Daniel_Radcliffe_SDCC_2014.jpg", "England" }
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "PictureUrl" },
                values: new object[,]
                {
                    { 1, "Batman", "Batman", "Male", "https://no.wikipedia.org/wiki/Batman#/media/Fil:Batman_(black_background).jpg" },
                    { 2, "Elessar", "Aragorn", "Male", "https://commons.wikimedia.org/wiki/File:Aragorn.png" },
                    { 3, "Mr Freeze", "Victor Fries", "Male", "https://en.wikipedia.org/wiki/Mr._Freeze#/media/File:Mr._Freeze_Batman_Annual_Vol_2_1.png" },
                    { 4, "Onkel Bob", "Terminator", "Male", "https://upload.wikimedia.org/wikipedia/en/7/70/Terminator1984movieposter.jpg" },
                    { 5, "The half blood prince", "Harry Potter", "Male", "https://image.side2.no/4458485.jpg?imageId=4458485&x=0&y=0&cropw=100&croph=100&width=480&height=270" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "An epic fantasy saga, based on the books by JRR Tolkien", "The lord of the rings " },
                    { 2, "The story about the masked vigilante", "Batman " },
                    { 3, "A terminator is sent back in time to save the future", "Terminator " },
                    { 4, "The story about a young wizard", "Harry Potter " }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genres", "MovieTitle", "PictureUrl", "ReleaseYear", "Trailer" },
                values: new object[,]
                {
                    { 1, "Peter Jackson", 1, "Adventure", "The fellowship of the ring", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i" },
                    { 2, "Joel Schumacker", 2, "Action", "Batman and Robin", "https://en.wikipedia.org/wiki/Batman_%26_Robin_(film)#/media/File:Batman_&_Robin_poster.jpg", new DateTime(1988, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=4RBXypX4qWI" },
                    { 3, "Christopher Nolan", 2, "Action", "The dark knight", "https://en.wikipedia.org/wiki/The_Dark_Knight_(film)#/media/File:Dark_Knight.jpg", new DateTime(2008, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i" },
                    { 4, "James Cameron", 3, "Action", "The Terminator", "https://upload.wikimedia.org/wikipedia/en/7/70/Terminator1984movieposter.jpg", new DateTime(1987, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=k64P4l2Wmeg" },
                    { 5, "Christoffer Columbus", 4, "Adventure", "Harry Potter and the philosophers stone", "https://upload.wikimedia.org/wikipedia/en/6/6b/Harry_Potter_and_the_Philosopher%27s_Stone_Book_Cover.jpg", new DateTime(2002, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=VyHV0BRtdxo" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "MovieId", "CharacterId", "ActorId", "PictureUrl" },
                values: new object[,]
                {
                    { 1, 2, 2, null },
                    { 2, 1, 1, null },
                    { 2, 3, 4, null },
                    { 3, 1, 3, null },
                    { 4, 4, 4, null },
                    { 5, 5, 5, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "MovieId", "CharacterId", "ActorId" },
                keyValues: new object[] { 1, 2, 2 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "MovieId", "CharacterId", "ActorId" },
                keyValues: new object[] { 2, 1, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "MovieId", "CharacterId", "ActorId" },
                keyValues: new object[] { 2, 3, 4 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "MovieId", "CharacterId", "ActorId" },
                keyValues: new object[] { 3, 1, 3 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "MovieId", "CharacterId", "ActorId" },
                keyValues: new object[] { 4, 4, 4 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "MovieId", "CharacterId", "ActorId" },
                keyValues: new object[] { 5, 5, 5 });

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 4);
        }
    }
}
