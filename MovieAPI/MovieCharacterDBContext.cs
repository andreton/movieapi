﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI
{
    public class MovieCharacterDBContext : DbContext
    {
        public MovieCharacterDBContext() { }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<MovieCharacter> MovieCharacter {get;set;}
        public DbSet<Franchise> Franchises { get; set; }

        public MovieCharacterDBContext(DbContextOptions<MovieCharacterDBContext> options) : base(options) { }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=PC7372\SQLEXPRESS;Initial Catalog=MovieAPIFinal;Integrated Security=True");
        }
        

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<MovieCharacter>().HasKey(pq => new { pq.MovieId, pq.CharacterId,pq.ActorId });
            //Setting up the relationsShips, moviecharacter is a binding solution between 3 different class with foreign keys
            modelbuilder.Entity<MovieCharacter>()
                .HasOne(mc => mc.Character)
                .WithMany(c => c.MovieCharacters)
                .HasForeignKey(mc => mc.CharacterId);

            modelbuilder.Entity<MovieCharacter>()
               .HasOne(ma => ma.Actor)
               .WithMany(t => t.MovieCharacters)
               .HasForeignKey(aa => aa.ActorId);

            modelbuilder.Entity<MovieCharacter>()
                .HasOne(me => me.Movie)
                .WithMany(m => m.MovieCharacters)
                .HasForeignKey(mm => mm.MovieId);



            modelbuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "The lord of the rings ", Description = "An epic fantasy saga, based on the books by JRR Tolkien" });
            modelbuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "Batman ", Description = "The story about the masked vigilante" });
            modelbuilder.Entity<Franchise>().HasData(new Franchise { Id = 3, Name = "Terminator ", Description = "A terminator is sent back in time to save the future" });
            modelbuilder.Entity<Franchise>().HasData(new Franchise { Id = 4, Name = "Harry Potter ", Description = "The story about a young wizard" });


            modelbuilder.Entity<Movie>().HasData(new Movie { Id = 1, MovieTitle = "The fellowship of the ring", Genres = "Adventure", ReleaseYear = new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Director = "Peter Jackson", PictureUrl = "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", Trailer = "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", FranchiseId = 1 });
            modelbuilder.Entity<Movie>().HasData(new Movie { Id = 2, MovieTitle = "Batman and Robin", Genres = "Action", ReleaseYear = new DateTime(1988, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Director = "Joel Schumacker", PictureUrl = "https://en.wikipedia.org/wiki/Batman_%26_Robin_(film)#/media/File:Batman_&_Robin_poster.jpg", Trailer = "https://www.youtube.com/watch?v=4RBXypX4qWI", FranchiseId = 2 });
            modelbuilder.Entity<Movie>().HasData(new Movie { Id = 3, MovieTitle = "The dark knight", Genres = "Action", ReleaseYear = new DateTime(2008, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Director = "Christopher Nolan", PictureUrl = "https://en.wikipedia.org/wiki/The_Dark_Knight_(film)#/media/File:Dark_Knight.jpg", Trailer = "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", FranchiseId = 2 });
            modelbuilder.Entity<Movie>().HasData(new Movie { Id = 4, MovieTitle = "The Terminator", Genres = "Action", ReleaseYear = new DateTime(1987, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Director = "James Cameron", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/7/70/Terminator1984movieposter.jpg", Trailer = "https://www.youtube.com/watch?v=k64P4l2Wmeg", FranchiseId = 3 });
            modelbuilder.Entity<Movie>().HasData(new Movie { Id = 5, MovieTitle = "Harry Potter and the philosophers stone", Genres = "Adventure", ReleaseYear = new DateTime(2002, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Director = "Christoffer Columbus", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/6/6b/Harry_Potter_and_the_Philosopher%27s_Stone_Book_Cover.jpg", Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo", FranchiseId = 4 });

            modelbuilder.Entity<Character>().HasData(new Character { Id = 1, FullName = "Batman", Alias = "Batman", Gender = "Male", PictureUrl = "https://no.wikipedia.org/wiki/Batman#/media/Fil:Batman_(black_background).jpg" });
            modelbuilder.Entity<Character>().HasData(new Character { Id = 2, FullName = "Aragorn", Alias = "Elessar", Gender = "Male", PictureUrl = "https://commons.wikimedia.org/wiki/File:Aragorn.png" });
            modelbuilder.Entity<Character>().HasData(new Character { Id = 3, FullName = "Victor Fries", Alias = "Mr Freeze", Gender = "Male", PictureUrl = "https://en.wikipedia.org/wiki/Mr._Freeze#/media/File:Mr._Freeze_Batman_Annual_Vol_2_1.png" });
            modelbuilder.Entity<Character>().HasData(new Character { Id = 4, FullName = "Terminator", Alias = "Onkel Bob", Gender = "Male", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/7/70/Terminator1984movieposter.jpg" });
            modelbuilder.Entity<Character>().HasData(new Character { Id = 5, FullName = "Harry Potter", Alias = "The half blood prince", Gender = "Male", PictureUrl = "https://image.side2.no/4458485.jpg?imageId=4458485&x=0&y=0&cropw=100&croph=100&width=480&height=270" });


            modelbuilder.Entity<Actor>().HasData(new Actor { Id = 1, FirstName = "George", LastName = "Clooney", Gender = "Male", DateOfBirth = new DateTime(1961, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), PlaceOfBirth = "Kentucky", Biography = "George Timothy Clooney (born May 6, 1961) is an American actor, film director, producer, and screenwriter. ", PictureUrl = "https://en.wikipedia.org/wiki/George_Clooney#/media/File:George_Clooney_2016.jpg" });
            modelbuilder.Entity<Actor>().HasData(new Actor { Id = 2, FirstName = "Viggo", LastName = "Mortensen", Gender = "Male", DateOfBirth = new DateTime(1957, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), PlaceOfBirth = "Manhattan", Biography = " Viggo Peter Mortensen jr. (født 20.oktober 1958 i New York City) er en dansk - amerikansk Oscar - og Golden Globe - nominert skuespiller, poet, fotograf og maler, mest kjent for rollen som «Aragorn» i Ringenes herre - filmtrilogien.", PictureUrl = "https://no.wikipedia.org/wiki/Viggo_Mortensen#/media/Fil:Viggo_Mortensen_Cannes_2016.jpg" });
            modelbuilder.Entity<Actor>().HasData(new Actor { Id = 3, FirstName = "Christian", LastName = "Bale", Gender = "Male", DateOfBirth = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), PlaceOfBirth = "HaverFordWest", Biography = "Christian Charles Philip Bale (født 30. januar 1974) er en britisk skuespiller. Han er kjent for sine roller i Terminator Salvation, American Psycho, Solens rike, Batman Begins, The Dark Knight, The Dark Knight Rises, The Fighter og American Hustle.", PictureUrl = "https://no.wikipedia.org/wiki/Christian_Bale#/media/Fil:Christian_Bale-7837.jpg" });
            modelbuilder.Entity<Actor>().HasData(new Actor { Id = 4, FirstName = "Arnold", LastName = "Scwarchneger", Gender = "Male", DateOfBirth = new DateTime(1957, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), PlaceOfBirth = "Austria", Biography = "Arnold Alois Schwarzenegger  is an Austrian-American actor, businessman, former politician and professional bodybuilder.", PictureUrl = "https://en.wikipedia.org/wiki/Arnold_Schwarzenegger#/media/File:Arnold_Schwarzenegger_by_Gage_Skidmore_4.jpg" });
            modelbuilder.Entity<Actor>().HasData(new Actor { Id = 5, FirstName = "Daniel", LastName = "Radcliffe", Gender = "Male", DateOfBirth = new DateTime(1989, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), PlaceOfBirth = "England", Biography = "Daniel Jacob Radcliffe (født 23. juli 1989 i Fulham i London) er en britisk skuespiller som er mest kjent for sin rolle som «Harry Potter», hovedrollefiguren i filmen Harry Potter og de vises stein og dens oppfølgere", PictureUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Daniel_Radcliffe_SDCC_2014.jpg/375px-Daniel_Radcliffe_SDCC_2014.jpg" });

            modelbuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { ActorId = 1, CharacterId = 1, MovieId = 2 });
            modelbuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { ActorId = 2, CharacterId = 2, MovieId = 1 });
            modelbuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { ActorId = 3, CharacterId = 1, MovieId = 3 });
            modelbuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { ActorId = 4, CharacterId = 3, MovieId = 2 });
            modelbuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { ActorId = 4, CharacterId = 4, MovieId = 4 });
            modelbuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { ActorId = 5, CharacterId = 5, MovieId = 5 });



        }
    }
}
