﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.DTOs.ActorsDTO
{
    public class MoviesByActorDto
    {
        /*Movies */
        public int MovieId { get; set; }
        public string MovieTitle { get; set; }

        /*Actors */

        public int ActorId { get; set; }
        public string FirstName { get; set; }       
        public string LastName { get; set; }

    }
}
