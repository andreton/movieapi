﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.DTOs.ActorsDTO
{
    public class CharactersPlayedByActorsDTO
    {
        /*Actors */
        public int ActorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        /*Characters*/
        public int CharacterId { get; set; }
        public string FullNameCharacter { get; set; }

   
    }
}
