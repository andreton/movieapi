﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.DTOs.ActorsDTO
{
    public class ActorListDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
    }
}
