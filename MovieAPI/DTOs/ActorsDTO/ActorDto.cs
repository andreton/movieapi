﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.DTOs.ActorsDTO
{
    public class ActorDto
    {

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Gender { get; set; }

        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Biography { get; set; }

        public string PictureUrl { get; set; }

    }
}
