﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.DTOs.MoviesDTO
{
    public class MovieFranchiseDto
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public string Genres { get; set; }
        public DateTime ReleaseYear { get; set; }
        public string Director { get; set; }
        public string PictureUrl { get; set; }
        public string Trailer { get; set; }
        public string FranchiseName { get; set; }
    }
    }

