﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.DTOs.MoviesDTO
{
    public class MovieListDto
    {
        /*Simple DTO*/
        public int Id { get; set; }
        public string MovieTitle { get; set; }
    }
}
