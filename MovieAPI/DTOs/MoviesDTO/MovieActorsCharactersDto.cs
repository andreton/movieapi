﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.DTOs.MoviesDTO
{
    public class MovieActorsCharactersDto
    {
        /*DTO for getting info on movies, actors and characters*/
        public int MovieId { get; set; }
        public string MovieTitle { get; set; }
        public DateTime ReleaseYear { get; set; }
        /*Actor first and last name*/
        public string FirstName { get; set; }
        public string LastName { get; set; }
        /*Character fullname and alias*/
        public string FullName { get; set; }
        public string Alias { get; set; }



    }
}
