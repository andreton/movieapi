﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.DTOs.CharactersDTO
{
    public class CharactersInAFranchiseDto
    {
        /*Characters*/
        public int CharacterId { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }

        /*Franchise*/
        public int FranchiseId { get; set; }
        public string FranchiseName { get; set; }
    }
}
