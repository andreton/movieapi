﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.DTOs.CharactersDTO
{
    public class ActorsWhoHavePlayedCharacterDto
    {
        /*Characters*/
        public int CharacterId { get; set; }
        public string FullNameCharacter { get; set; }

        /*Actor*/
        public int ActorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    
    }
}
