﻿using AutoMapper;
using MovieAPI.DTOs.ActorsDTO;
using MovieAPI.DTOs.CharactersDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Profiles
{
    public class ActorProfile :Profile
    {
        public ActorProfile()
        {
            /*Some default DTO's*/
            CreateMap<Actor, ActorDto>();
            CreateMap<Actor, ActorListDto>().ReverseMap();
            /*Mapper to display the full name of the character and the first and last name*/
            CreateMap<MovieCharacter, CharactersPlayedByActorsDTO>()
                .ForMember(c => c.FullNameCharacter, opt => opt.MapFrom(c => c.Character.FullName))
                .ForMember(a => a.FirstName, opt => opt.MapFrom(a => a.Actor.FirstName))
                .ForMember(a => a.LastName, opt => opt.MapFrom(a => a.Actor.LastName));
            /*Mapper to display the movies played by an character*/
            CreateMap<MovieCharacter, MoviesByActorDto>()
                .ForMember(m => m.MovieTitle, opt => opt.MapFrom(m => m.Movie.MovieTitle))
                .ForMember(a => a.FirstName, opt => opt.MapFrom(a => a.Actor.FirstName))
                .ForMember(a => a.LastName, opt => opt.MapFrom(a => a.Actor.LastName));
        }

    }
}
