﻿using AutoMapper;
using MovieAPI.DTOs.MoviesDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Profiles
{
    public class MovieProfile :Profile
    {
        public MovieProfile()
        {
            /*Some default mapping*/
            CreateMap<Movie, MovieDto>();
            CreateMap<Movie, MovieListDto>().ReverseMap();
            /*Mapping for movies based on franchise */
            CreateMap<Movie, MovieFranchiseDto>().ForMember(mvdto=>mvdto.FranchiseName, opt=>opt.MapFrom(a=>a.Franchise.Name));
            /*Mapping for characters and actors from movies*/
            CreateMap<MovieCharacter, MovieActorsCharactersDto>()
                .ForMember(mc => mc.FullName, opt => opt.MapFrom(mc => mc.Character.FullName))
                .ForMember(mc => mc.Alias, opt => opt.MapFrom(mc => mc.Character.Alias))
                .ForMember(ma => ma.FirstName, opt => opt.MapFrom(ma => ma.Actor.FirstName))
                .ForMember(ma => ma.LastName, opt => opt.MapFrom(ma => ma.Actor.LastName))
                .ForMember(m => m.MovieTitle, opt => opt.MapFrom(m => m.Movie.MovieTitle))
                .ForMember(m => m.ReleaseYear, opt => opt.MapFrom(m => m.Movie.ReleaseYear))
                ;
        }
 
    }
}
