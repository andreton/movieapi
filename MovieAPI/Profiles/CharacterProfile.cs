﻿using AutoMapper;
using MovieAPI.DTOs.ActorsDTO;
using MovieAPI.DTOs.CharactersDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDto>();
            CreateMap<Character, CharacterListDto>().ReverseMap();
            /*Mapper for displaying the actor who have played a particular character*/
            CreateMap<MovieCharacter, ActorsWhoHavePlayedCharacterDto>()
                .ForMember(c => c.FullNameCharacter, opt => opt.MapFrom(c => c.Character.FullName))
                .ForMember(a => a.FirstName, opt => opt.MapFrom(a => a.Actor.FirstName))
                .ForMember(a => a.LastName, opt => opt.MapFrom(a => a.Actor.LastName));
            /*Mapper for displaying the info about the character in a franchise*/
            CreateMap<MovieCharacter, CharactersInAFranchiseDto>()
                .ForMember(c => c.FullName, opt => opt.MapFrom(c => c.Character.FullName))
                .ForMember(c => c.Alias, opt => opt.MapFrom(c => c.Character.Alias))
                .ForMember(m => m.FranchiseName, opt => opt.MapFrom(f => f.Movie.Franchise.Name));
        }

    }
}
