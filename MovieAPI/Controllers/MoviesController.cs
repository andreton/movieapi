﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.DTOs.MoviesDTO;



namespace MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharacterDBContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieCharacterDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Movie>>> GetMovie()
        {

            var movies= await _context.Movie.ToListAsync();
            var dto = _mapper.Map<List<MovieDto>>(movies);
            return Ok(dto);
        }

        /*Method for getting movie based on franchise, calling a custom DTO to list movieInfo and FranchiseName*/
        [HttpGet("{franchiseId}/movie")]
        public async Task<ActionResult<List<MovieFranchiseDto>>> GetMoviesFromFranchise(int franchiseId)
        {
            //Calling the method async with the await command 
            var movies = await  _context.Movie.Where(m => m.FranchiseId == franchiseId).Include(fr=>fr.Franchise).ToListAsync() ;
            var dto= _mapper.Map<List<MovieFranchiseDto>>(movies);
      
            return Ok(dto);

        }

        /*Method for returning the actor and character based on movie*/
        [HttpGet("{movieId}/characters-actors")]
        public async Task<ActionResult<MovieActorsCharactersDto>> charactersAndActorsFromMovies(int movieId)
        {
            /*
            var charactersAndActors = await _context.Movie
                .Include(c => c.MovieCharacters)
                .ThenInclude(c => c.Actor)
                .Include(c => c.MovieCharacters)
                .ThenInclude(c => c.Character)
                .Where(c => c.Id == movieId).ToListAsync();
            */
            var charactersAndActors = await _context.MovieCharacter
                .Include(c => c.Character)
                .Include(m => m.Movie)
                .Include(a => a.Actor)
                .Where(m => m.MovieId == movieId).ToListAsync();
            var dto = _mapper.Map<List<MovieActorsCharactersDto>>(charactersAndActors);
            return Ok(dto);
        }




        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDto>> GetMovie(int id)
        {
            var movie = await _context.Movie.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }
            var dto = _mapper.Map<MovieDto>(movie);

            return Ok(dto);
        }

        // PUT: api/Movies/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Movies
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(Movie movie)
        {
            _context.Movie.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Movie>> DeleteMovie(int id)
        {
            var movie = await _context.Movie.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movie.Remove(movie);
            await _context.SaveChangesAsync();

            return movie;
        }

        private bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.Id == id);
        }
    }
}
