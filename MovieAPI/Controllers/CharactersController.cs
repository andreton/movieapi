﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.DTOs.CharactersDTO;

namespace MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterDBContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharacterDBContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Character>>> GetCharacters()
        {
            var characters = await _context.Characters.ToListAsync();
            var dto = _mapper.Map<List<CharacterDto>>(characters);

            return Ok(dto);
        }

        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Character>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            var dto = _mapper.Map<CharacterDto>(character);

            if (character == null)
            {
                return NotFound();
            }

            return Ok(dto);
        }

        /*Get all actors who have played a specific character*/
        [HttpGet("{IdCharacter}/actors")]
        public async Task<ActionResult<ActorsWhoHavePlayedCharacterDto>> getActorsBasedOnCharacters(int IdCharacter) {
            var charactersAndActors = await _context.MovieCharacter.Include(a => a.Actor).Include(c => c.Character).Where(c =>IdCharacter == c.CharacterId).ToListAsync();
                /* Keep this query for reference, does the same thing
                 * _context.Characters.Where(c => c.Id == id).SelectMany(cm => cm.MovieCharacters).Select(cma => cma.Actor);
                */
            var dto= _mapper.Map<List<ActorsWhoHavePlayedCharacterDto>>(charactersAndActors);
            return Ok(dto);
        }


        // PUT: api/Characters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Character>> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return character;
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
