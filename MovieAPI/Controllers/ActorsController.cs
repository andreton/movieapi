﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.DTOs.ActorsDTO;
using MovieAPI.DTOs.CharactersDTO;

namespace MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly MovieCharacterDBContext _context;
        private readonly IMapper _mapper;

        public ActorsController(MovieCharacterDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Actors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Actor>>> GetActors()
        {
            var actors= await _context.Actors.ToListAsync();
            var dto = _mapper.Map<List<ActorDto>>(actors);
            return  Ok(dto);
        }

        // GET: api/Actors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Actor>> GetActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            var dto = _mapper.Map<ActorDto>(actor);
            if (actor == null)
            {
                return NotFound();
            }

            return Ok(dto);
        }
        // GET: All characters an actor have played
        [HttpGet("{ActorId}/characters")]
        public async Task<ActionResult<List<CharactersPlayedByActorsDTO>>> getCharacters(int ActorId)
        {
            var actors = await _context.MovieCharacter.Include(c => c.Actor).Include(c => c.Character).Where(c => c.ActorId== ActorId).ToListAsync();
            /*  Different method of achiving the same, storing it for later reference
             * var actors =await _context.Actors.Where(c => c.Id==ActorId).Include(mc=>mc.MovieCharacters).ThenInclude(mcc=>mcc.Character).SelectMany(c=>c.MovieCharacters).Select(c=>c.Character).ToListAsync();
            */
            var dto = _mapper.Map<List<CharactersPlayedByActorsDTO>>(actors);
            return Ok(dto);  
        }

        //Get : Getting the movies a character has played in  
      
        [HttpGet("{ActorId}/movies")]
        public async Task<ActionResult<MoviesByActorDto>> getMoviesOfActor(int ActorId)
        {
            var characters =await _context.MovieCharacter.Include(m => m.Movie).Include(A => A.Actor).Where(a => a.ActorId == ActorId).ToListAsync();

            var dto = _mapper.Map<List<MoviesByActorDto>>(characters);
            return  Ok(dto);
        }

        // PUT: api/Actors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Actor>> PostActor(Actor actor)
        {
            _context.Actors.Add(actor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = actor.Id }, actor);
        }

        // DELETE: api/Actors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Actor>> DeleteActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return actor;
        }

        private bool ActorExists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }
    }
}
